package com.chemique.QuestionAnswering;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Question extends AppCompatActivity {


    String tag_ = "QST_GEN_RESULT_ACT:";
    private TextView ques;
    private EditText ip;
    private Button get;
    private CheckBox ans;
    private CoordinatorLayout mCLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        Context mContext = getApplicationContext();

        ques = findViewById(R.id.text_question);
        ip = findViewById(R.id.ipAddr);
        get = findViewById(R.id.connectApi);
        ans = findViewById(R.id.ans1);

        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getApi();
            }
        });
    }


    public void getApi() {

        //String url = "http://192.168.32.136:5010/incomes";
        String url = ip.getText().toString();
        ques.setText(" ");
        ans.setText(" ");

        RequestQueue requstQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(tag_,response.toString());
                        try {
                            JSONObject length = response.getJSONObject("question");
                            String qst = response.getJSONObject("question").getString("0");
                            //  tv_Response.setText(value);
                            //System.out.println(qst);
                            //ques.setText(" ");
                            ques.setText(qst);

                            String answ = response.getJSONObject("answer").getString("0");
                            //System.out.println(answ);
                            //ans.setText(" ");
                            ans.setText(answ);
                            //System.out.println(length.length());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(tag_, error.getMessage());
                    }
                }
        ) {
            //here I want to post data to sever
        };

        jsonobj.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requstQueue.add(jsonobj);
    }


}
